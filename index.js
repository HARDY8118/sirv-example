require('dotenv').config()
const express = require('express')
const multer = require('multer')
const axios = require('axios')
const fs = require('fs')

const app = express()

const upload = multer({
    storage: multer.diskStorage({
        destination: (req, file, callback) => {
            callback(null, 'temp')
        },
        filename: (req, file, callback) => {
            callback(null, file.originalname)
        }
    })
});

const PORT = process.env.PORT || 5000

app.set('view engine', 'ejs')

app.get('/', (req, res) => {
    return res.render('index')
})

app.post('/upload', upload.single('imageFile'), (req, res) => {
    axios.post('https://api.sirv.com/v2/token', {
        'clientId': process.env.clientId,
        'clientSecret': process.env.clientSecret
    }, {
        headers: {
            'content-type': 'application/json'
        }
    })
        .then(response => response.data.token)
        .then(token => {
            fs.readFile(req.file.path, (err, image) => {
                if (err) {
                    throw err
                }
                axios.post('https://api.sirv.com/v2/files/upload', image, {
                    params: {
                        'filename': '/' + req.file.originalname
                    },
                    headers: {
                        'content-type': req.file.mimetype,
                        authorization: `Bearer ${token}`
                    },
                })
                    .then(response => {
                        res.status(response.status).send(response.statusText)
                    })
                    .catch(error => {
                        res.status(error.response.status).send(error.response.statusText)
                    })
                    // Optional delete file after upload
                    .finally(() => {
                        try {
                            fs.unlinkSync(req.file.path)
                        }
                        catch{
                            // File does not exists
                        }
                    })
            })
        })
        .catch(err => {
            throw err
        })
})

app.listen(PORT, () => {
    console.log(`http://127.0.0.1:${PORT}`)
})